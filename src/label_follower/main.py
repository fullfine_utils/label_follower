from mongo_utils.spotify.sp_mongo import MongoSPManager
from spotify_utils.sp_manager import SpotifyManager


class LabelFollower:

    def __init__(self) -> None:
        self.sp = SpotifyManager()
        self.db = MongoSPManager()

    def _get_new_albums(self, old_albums, spotify_albums):
        new_albums = []
        for salbum in spotify_albums:
            if salbum not in old_albums:
                new_albums.append(salbum)
        return new_albums

    def _add_new_albums_to_db(self, new_albums):
        for album in new_albums:
            self.db.add_album(album)

    def _add_new_albums_to_playlist_label(self, label, new_albums):
        playlist = self.sp.get_playlist_by_id(label["playlist_id"])
        for album in new_albums:
            tracks = self.sp.get_tracks_from_album(album["id"])
            playlist = self.sp.add_tracks_to_playlist(playlist, tracks)

    def _process_label(self, label):
        old_albums = label["albums"]
        spotify_albums = self.sp.get_albums_from_label(label["name"])
        new_albums = self._get_new_albums(old_albums, spotify_albums)

        for album in new_albums:
            self.db.add_album(album)


    def sync_labels(self):
        labels = self.db.get_labels()
        for label in labels:
            self._process_label(label)


    def _add_label(self, label_name):
        label = {"name": label_name}

def main():
    label_follower = LabelFollower()
    label_follower._add_label()
    label_follower.sync_labels()


if __name__ == '__main__':
    main()